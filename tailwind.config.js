/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./app/**/templates/**/*.{html,js,twig}"],
  theme: {
    extend: {},
  },
  plugins: [],
}
